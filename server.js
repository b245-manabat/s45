const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


const userRoutes = require("./Routes/userRoutes.js");
const productRoutes = require("./Routes/productRoutes.js");
const orderRoutes = require("./Routes/orderRoutes.js");


const port = 4000;
const app = express();
	mongoose.set('strictQuery', true);
	mongoose.connect("mongodb+srv://admin:admin@batch245-manabat.mca9yeu.mongodb.net/Capstone2?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	})
	let db = mongoose.connection;
	db.on("error", console.error.bind(console, "Connection Error!"));
	db.once("open", () => {console.log('We are connected to the cloud!')});

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

//routing
app.use("/user", userRoutes);
app.use("/product", productRoutes);
app.use("/order", orderRoutes);

app.listen(port, ()=> console.log(`Server is running at Port ${port}`))