const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const orderController = require("../Controllers/orderController");

router.post("/add", orderController.addOrder);
router.get("/view", orderController.viewOrder);
router.get("/all", auth.verify, orderController.allOrder);

module.exports = router;