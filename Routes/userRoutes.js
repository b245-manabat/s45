const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const userController = require("../Controllers/userController.js")

router.post("/register", userController.userRegistration);
router.post("/login", userController.userAuthentication);
router.post("/details", userController.userProfile);
router.put("/admin", auth.verify, userController.userAdmin);

module.exports = router;