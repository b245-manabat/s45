const Product = require("../Models/productsSchema.js");
const auth = require("../auth.js");
const mongoose = require("mongoose");

module.exports.addProduct = (request, response) =>{
	let input = request.body;
	const userData = auth.decode(request.headers.authorization);

		if(userData.isAdmin) {let newProduct = new Product({
			name: input.name,
			description: input.description,
			price: input.price
		});
	
		return newProduct.save()
		.then(product =>{
			console.log(product);
			response.send(product);
		})
		.catch(error =>{
			console.log(error);
			response.send(false);
		})}
		else {
			return response.send("You are not an admin.");
		}
}

module.exports.viewProducts = (request, response) => {
	const product = request.body;
	let isActive = product.isActive;

	Product.find({isActive: true})
	.then(result =>{
		return response.send(result);
	})
	.catch(error =>{
		console.log(error);
		response.send(false);
	})
}

module.exports.viewProduct = (request, response) => {
	const product = request.body;

	Product.findOne({name: product.name})
	.then(result =>{
		return response.send(result);
	})
	.catch(error =>{
		console.log(error);
		response.send(false);
	})
}

module.exports.updateProduct = (request, response) => {
	let product = request.body;
	const userData = auth.decode(request.headers.authorization);

		if(userData.isAdmin) {
			Product.findOneAndUpdate({name: product.name},{
				description: product.description,
				price: product.price
			},{new:true})
			.then(result =>{
				return response.send(result);
			})
			.catch(error =>{
				console.log(error);
				response.send(false);
			})
		}
		else {
			return response.send("You are not an admin.");
		}
}

module.exports.archiveProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin) {
		let product = request.body;
		Product.findOneAndUpdate({name: product.name},{isActive: false},{new:true})
		.then(result =>{
			return response.send(`The product ${product.name} is now archived.`);
		})
		.catch(error =>{
			console.log(error);
			response.send(false);
		})
	}
	else {
		return response.send("You are not an admin.");
	}
}