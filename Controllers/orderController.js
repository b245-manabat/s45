const Order = require("../Models/ordersSchema.js");
const Product = require("../Models/productsSchema.js");
const auth = require("../auth.js");
const mongoose = require("mongoose");



module.exports.addOrder = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	let user = userData._id;
		const input = request.body;

		Product.findOne({name: input.products})
		.then(result => {
			let newOrder = new Order({
				userId : user,
				products : {productId:result._id,quantity:input.quantity},
				totalAmount : (result.price*input.quantity)
			})

			newOrder.save()
			.then(save => {
				return response.send("Order received!")
			})
			.catch(error => {
				return response.send(error)
			})
		})
		.catch(error => {
			return response.send("No product found!")
		})
}


module.exports.viewOrder = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	let user = userData._id;
	
	Order.find({userId: user})
	.then(result =>{
		return response.send(result);
	})
	.catch(error =>{
		console.log(error);
		response.send(false);
	})
}


module.exports.allOrder = (request, response) => {
	const userData = auth.decode(request.headers.authorization);


	if(userData.isAdmin) {
		Order.find({})
		.then(result =>{
		return response.send(result);
		})
		.catch(error =>{
			console.log(error);
			response.send(false);
		})
	}
	else {
		return response.send("You are not an admin.");
	}
}