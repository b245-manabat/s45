const mongoose = require("mongoose");
const User = require("../Models/usersSchema.js")
const bcrypt = require("bcrypt");
const auth = require("../auth.js");



module.exports.userRegistration = (request, response) => {
		const input = request.body;

		User.findOne({email: input.email})
		.then(result => {
			if(result !== null){
				return response.send("The email is already taken!")
			}else{
				let newUser = new User({
					email: input.email,
					password: bcrypt.hashSync(input.password, 10),
				})

				newUser.save()
				.then(save => {
					return response.send("You are now registered to our website!")
				})
				.catch(error => {
					return response.send(error)
				})
			}
		})
		.catch(error => {
			return response.send(error)
		})
}

module.exports.userAuthentication = (request, response) => {
		let input = request.body;

		User.findOne({email: input.email})
		.then(result => {
			if(result === null){
				return response.send("Email is not yet registered. Register now!")
			}else{
				const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

				if(isPasswordCorrect){
					return response.send({auth: auth.createAccessToken(result)});
				}else{
					return response.send("Password is incorrect!")
				}
			}
		})
		.catch(error => {
			return response.send(error);
		})
}

module.exports.userProfile = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	return User.findById(userData._id).then(result =>{;

		return response.send({
			...result._doc,
			password: "confidential"
		});
	})
}

module.exports.userAdmin = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin) {
		let user = request.body;
		User.findOneAndUpdate({email: user.email},{isAdmin: true},{new:true})
		.then(result =>{
			return response.send(`The user ${user.email} is now an admin.`);
		})
		.catch(error =>{
			console.log(error);
			response.send(false);
		})
	}
	else {
		return response.send("You are not an admin.");
	}
}